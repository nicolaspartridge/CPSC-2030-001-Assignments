let characters = [
	{id:1,name:"Claude Wallace",side:"Edinburgh Army",unit:"Ranger Corps, Squad E",rank:"First Lieutenant",role:"Tank Commander",description:"Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.",imageUrl:"http://valkyria.sega.com/img/character/chara01.png"},
	{id:2,name:"Riley Miller",side:"Edinburgh Army",unit:"Federate Joint Ops",rank:"Second Lieutenant",role:"Artillery Advisor",description:"Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones.",imageUrl:"http://valkyria.sega.com/img/character/chara02.png"},
	{id:3,name:"Raz",side:"Edinburgh Army",unit:"Ranger Corps, Squad E",rank:"Sergeant",role:"Fireteam Leader",description:"Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he's invincible.",imageUrl:"http://valkyria.sega.com/img/character/chara03.png"},
	{id:4,name:"Kai Schulen",side:"Edinburgh Army",unit:"Ranger Corps, Squad E",rank:"Sergeant Major",role:"Fireteam Leader",description:"Born in the Gallian city of Hafen, this cool and collected sharpshooter has earned the codename 'Deadeye Kai.' Along with her childhood friends, she joined a foreign military to take the fight to the Empire. She loves fresh-baked bread, almost to a fault.",imageUrl:"http://valkyria.sega.com/img/character/chara04.png"},
	{id:5,name:"Minvera Victor",side:"Edinburgh Army",unit:"Ranger Corps, Squad F",rank:"First Lieutenant",role:"Senior Commander",description:"Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division's squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals.",imageUrl:"http://valkyria.sega.com/img/character/chara11.png"},
	{id:6,name:"Karen Stuart",side:"Edinburgh Army",unit:"Squad E",rank:"Corporal",role:"Combat EMT",description:"Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation's military to support her growing household.",imageUrl:"http://valkyria.sega.com/img/character/chara12.png"},
	{id:7,name:"Ragnarok",side:"Edinburgh Army",unit:"Squad E",rank:"K-9 Unit",role:"Mascot",description:"Once a stray, this good good boy is lovingly referred to as 'Rags.'As a K-9 unit, he's a brave and intelligent rescue dog who's always willing to lend a helping paw. When the going gets tough, the tough get ruff.",imageUrl:"http://valkyria.sega.com/img/character/chara13.png"},
	{id:8,name:"Miles Arbeck",side:"Edinburgh Army",unit:"Ranger Corps, Squad E",rank:"Sergeant",role:"Tank Operator",description:"Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby.",imageUrl:"http://valkyria.sega.com/img/character/chara15.png"},
	{id:9,name:"Dan Bentley",side:"Edinburgh Army",unit:"Ranger Corps, Squad E",rank:"Private First Class",role:"APC Operator",description:"Born in the United States of Vinland, this driver loves armored personnel carriers with a passion. His skill behind the wheel is matched only by his way with a wrench. Though not much of a talker, he takes pride in carrying his teammates through combat.",imageUrl:"http://valkyria.sega.com/img/character/chara16.png"},
	{id:10,name:"Ronald Albee",side:"Edinburgh Army",unit:"Ranger Corps, Squad F",rank:"Second Lieutenant",role:"Tank Operator",description:"Born in the United Kingdom of Edinburgh, this stern driver was Minerva Victor's underclassman at the military academy. Upon being assigned to Squad F, he swore an oath of fealty to Lt. Victor, and takes great satisfaction in upholding her chivalric code.",imageUrl:"http://valkyria.sega.com/img/character/chara17.png"},
]

let squad = [];

window.onload = function() {
	for(i = 0; i < 10; i++){
		document.getElementById(i+1).innerHTML = characters[i].name;
	}

	hoverCharList( document.getElementById(1));
}

function selectChar(elm) {
	for(i = 0; i < 10; i++){
		document.getElementById(i+1).classList.remove('bold');
	}

	if (!elm.classList.contains('bold')) {
		elm.classList.add('bold');
	}

	if (squad.length < 5) {
		if (!squad.includes(characters[elm.id-1])) {
			squad.push(characters[elm.id-1]);
		}
	}

	for(i = 0; i < squad.length; i++){
		document.getElementById(i+1+'s').innerHTML = squad[i].name;
	}
}

function removeFromSquad(elm) {
	let name = elm.innerHTML;
	let id = String(elm.id).substring(0, 1);
	
	squad.splice(id-1, 1);
	elm.innerHTML = '';

	for(i = 0; i < squad.length; i++){
		document.getElementById(i+1+'s').innerHTML = squad[i].name;
	}

	document.getElementById(squad.length+1+'s').innerHTML = '';	
}

function hoverCharList(elm) {
	let charInfo = document.getElementById('char-info');
	let img = document.getElementById('img');

	let id = (elm.id-1);

	img.src = characters[id].imageUrl;

	charInfo.innerHTML =  characters[id].name + "<hr>" +
						characters[id].side + "<br>" +
						characters[id].unit + "<br>" +
						characters[id].rank + "<br>" +
						characters[id].role + "<br>" +
						characters[id].description + "<hr><br>";
}

function hoverCharSquad(elm) {
	let charInfo = document.getElementById('char-info');
	let img = document.getElementById('img');

	let id = String(elm.id).substring(0, 1);

	img.src = characters[id].imageUrl;

	charInfo.innerHTML =  characters[id].name + "<hr>" +
						characters[id].side + "<br>" +
						characters[id].unit + "<br>" +
						characters[id].rank + "<br>" +
						characters[id].role + "<br>" +
						characters[id].description + "<hr><br>";
}