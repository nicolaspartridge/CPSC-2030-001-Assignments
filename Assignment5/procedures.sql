DELIMITER $$
CREATE PROCEDURE `pokedexGetAll`()
begin
	select *
	from pokemon;
end$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `pokedexGetByType`(IN `pType` VARCHAR(10), IN `showall` INT)
    NO SQL
if (showall = 1) then
	select *
	from pokemon
	where type = pType OR type2 = pType;
else
	select *
	from pokemon;
end if$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `pokedexGetByNum`(IN `id` INT)
    NO SQL
BEGIN
	select *
	from pokemon
	where num = id;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `pokedexGetAttributes`(IN `pType` VARCHAR(10), IN `pType2` VARCHAR(10))
    NO SQL
begin
	select strong_against, weak_against, vulnerable, resistant
	from strong_against
    inner join weak_against
 	on strong_against.type = weak_against.type
    inner join resistant
    on strong_against.type = resistant.type
    inner join vulnerable
    on strong_against.type = vulnerable.type
    where strong_against.type = pType OR strong_against.type = pType2;
       
    
end$$
DELIMITER ;