CREATE DATABASE pokedex;

CREATE TABLE pokemon (
	num INT,
    name VARCHAR(15),
    type VARCHAR(10),
    type2 VARCHAR(10),
    hp INT,
    atk INT,
    def INT,
    sAT INT,
    sDf INT,
    spd INT,
    BST INT
);

CREATE TABLE strong_against (
    type VARCHAR(10),
    strong_against VARCHAR(100)
);

CREATE TABLE weak_against (
    type VARCHAR(10),
    weak_against VARCHAR(100)
);

CREATE TABLE resistant (
    type VARCHAR(10),
    resistant VARCHAR(100)
);

CREATE TABLE vulnerable (
    type VARCHAR(10),
    vulnerable VARCHAR(100)
);

INSERT INTO pokemon VALUES (387,'Turtwig','Grass',NULL,55,68,64,45,55,31,318);
INSERT INTO pokemon VALUES (388,'Grotle','Grass',NULL,75,89,85,55,65,36,405);
INSERT INTO pokemon VALUES (389,'Torterra','Grass','Ground',95,109,105,75,85,56,525);
INSERT INTO pokemon VALUES (390,'Chimchar','Fire',NULL,44,58,44,58,44,61,309);
INSERT INTO pokemon VALUES (391,'Monferno','Fire','Fighting',64,78,52,78,52,81,405);
INSERT INTO pokemon VALUES (392,'Infernape','Fire','Fighting',76,104,71,104,71,108,534);
INSERT INTO pokemon VALUES (393,'Piplup','Water',NULL,53,51,53,61,56,40,314);
INSERT INTO pokemon VALUES (394,'Prinplup','Water',NULL,64,66,68,81,76,50,405);
INSERT INTO pokemon VALUES (395,'Empoleon','Water','Steel',84,86,88,111,101,60,530);
INSERT INTO pokemon VALUES (396,'Starly','Normal','Flying',40,55,30,30,30,60,245);
INSERT INTO pokemon VALUES (397,'Staravia','Normal','Flying',55,75,50,40,40,80,340);
INSERT INTO pokemon VALUES (398,'Staraptor','Normal','Flying',85,120,70,50,60,100,485);
INSERT INTO pokemon VALUES (399,'Bidoof','Normal',NULL,59,45,40,35,40,31,250);
INSERT INTO pokemon VALUES (400,'Bibarel','Normal','Water',79,85,60,55,60,71,410);
INSERT INTO pokemon VALUES (401,'Kricketot','Bug',NULL,37,25,41,25,41,25,194);
INSERT INTO pokemon VALUES (402,'Kricketune','Bug',NULL,77,85,51,55,51,65,384);
INSERT INTO pokemon VALUES (403,'Shinx','Electric',NULL,45,65,34,40,34,45,263);
INSERT INTO pokemon VALUES (404,'Luxio','Electric',NULL,60,85,49,60,49,60,363);
INSERT INTO pokemon VALUES (405,'Luxray','Electric',NULL,80,120,79,95,79,70,523);
INSERT INTO pokemon VALUES (406,'Budew','Grass','Poison',40,30,35,50,70,55,280);
INSERT INTO pokemon VALUES (407,'Roserade','Grass','Poison',60,70,65,125,105,90,515);
INSERT INTO pokemon VALUES (408,'Cranidos','Rock',NULL,67,125,40,30,30,58,350);
INSERT INTO pokemon VALUES (409,'Rampardos','Rock',NULL,97,165,60,65,50,58,495);
INSERT INTO pokemon VALUES (410,'Shieldon','Rock','Steel',30,42,118,42,88,30,350);
INSERT INTO pokemon VALUES (411,'Bastiodon','Rock','Steel',60,52,168,47,138,30,495);
INSERT INTO pokemon VALUES (412,'Burmy','Bug',NULL,40,29,45,29,45,36,224);
INSERT INTO pokemon VALUES (413,'Wormadam','Bug','Grass',60,79,105,59,85,36,424);
INSERT INTO pokemon VALUES (414,'Mothim','Bug','Flying',70,94,50,94,50,66,424);
INSERT INTO pokemon VALUES (415,'Combee','Bug','Flying',30,30,42,40,42,70,244);
INSERT INTO pokemon VALUES (416,'Vespiquen','Bug','Flying',70,80,102,80,102,40,474);
INSERT INTO pokemon VALUES (417,'Pachirisu','Electric',NULL,60,45,70,45,90,95,405);
INSERT INTO pokemon VALUES (418,'Buizel','Water',NULL,55,65,35,60,30,85,330);
INSERT INTO pokemon VALUES (419,'Floatzel','Water',NULL,85,105,55,85,50,115,495);
INSERT INTO pokemon VALUES (420,'Cherubi','Grass',NULL,45,35,45,62,53,35,275);
INSERT INTO pokemon VALUES (421,'Cherrim','Grass',NULL,70,60,70,87,78,8,450);
INSERT INTO pokemon VALUES (422,'Shellos','Water',NULL,76,48,48,57,62,34,325);
INSERT INTO pokemon VALUES (423,'Gastrodon','Water','Ground',111,83,68,92,82,39,475);
INSERT INTO pokemon VALUES (424,'Ambipom','Normal',NULL,75,100,66,60,66,115,482);
INSERT INTO pokemon VALUES (425,'Drifloon','Ghost','Flying',90,50,34,60,44,70,348);
INSERT INTO pokemon VALUES (426,'Drifblim','Ghost','Flying',150,80,44,90,54,80,498);
INSERT INTO pokemon VALUES (427,'Buneary','Normal',NULL,55,66,44,44,56,85,350);
INSERT INTO pokemon VALUES (428,'Lopunny','Normal',NULL,65,76,84,54,96,105,480);
INSERT INTO pokemon VALUES (428,'Mega Lopunny','Normal','Fightning',65,136,94,54,96,135,580);
INSERT INTO pokemon VALUES (429,'Mismagius','Ghost',NULL,60,60,60,105,105,105,495);
INSERT INTO pokemon VALUES (430,'Honchkrow','Dark','Flying',100,125,52,105,52,71,505);
INSERT INTO pokemon VALUES (431,'Glameow','Normal',NULL,49,55,42,42,37,85,310);
INSERT INTO pokemon VALUES (432,'Purugly','Normal',NULL,71,82,64,64,59,112,452);
INSERT INTO pokemon VALUES (433,'Chingling','Psychic',NULL,45,30,50,65,50,45,285);
INSERT INTO pokemon VALUES (434,'Stunky','Poison','Dark',63,63,47,41,41,74,329);
INSERT INTO pokemon VALUES (435,'Skuntank','Poison','Dark',103,93,67,71,61,84,479);
INSERT INTO pokemon VALUES (436,'Bronzor','Steel','Psychic',57,24,86,24,86,23,300);
INSERT INTO pokemon VALUES (437,'Bronzong','Steel','Psychic',67,89,116,79,116,33,500);
INSERT INTO pokemon VALUES (438,'Bonsly','Rock',NULL,50,80,95,10,45,10,290);
INSERT INTO pokemon VALUES (439,'Mime Jr.','Psychic','Fairy',20,25,45,70,90,60,310);
INSERT INTO pokemon VALUES (440,'Happiny','Normal',NULL,100,5,5,15,65,30,220);
INSERT INTO pokemon VALUES (441,'Chatot','Normal','Flying',76,65,45,92,42,91,411);
INSERT INTO pokemon VALUES (442,'Spiritomb','Ghost','Dark',50,92,108,92,108,35,485);
INSERT INTO pokemon VALUES (443,'Gible','Dragon','Ground',58,70,45,40,45,42,300);
INSERT INTO pokemon VALUES (444,'Gabite','Dragon','Ground',68,90,65,50,55,82,410);
INSERT INTO pokemon VALUES (445,'Garchomp','Dragon','Ground',108,130,95,80,85,102,600);
INSERT INTO pokemon VALUES (445,'Mega Garchomp','Dragon','Ground',108,170,115,120,95,92,700);
INSERT INTO pokemon VALUES (446,'Munchlax','Normal',NULL,135,85,40,40,85,5,390);
INSERT INTO pokemon VALUES (447,'Riolu','Fightning',NULL,40,70,40,35,40,60,285);
INSERT INTO pokemon VALUES (448,'Lucario','Fightning','Steel',70,110,70,115,70,90,525);
INSERT INTO pokemon VALUES (448,'Mega Lucario','Fightning','Steel',70,145,88,140,70,112,625);
INSERT INTO pokemon VALUES (449,'Hippopotas','Ground',NULL,68,72,78,38,42,32,330);
INSERT INTO pokemon VALUES (450,'Hippowdon','Ground',NULL,108,112,118,68,72,47,525);
INSERT INTO pokemon VALUES (451,'Skorupi','Poison','Bug',40,50,90,30,55,65,330);
INSERT INTO pokemon VALUES (452,'Drapion','Poison','Dark',70,90,110,60,75,95,500);
INSERT INTO pokemon VALUES (453,'Croagunk','Poison','Fightning',48,61,40,61,40,50,300);
INSERT INTO pokemon VALUES (454,'Toxicroak','Poison','Fightning',83,106,65,86,65,85,490);
INSERT INTO pokemon VALUES (455,'Carnivine','Grass',NULL,74,100,72,90,72,46,454);
INSERT INTO pokemon VALUES (456,'Finneon','Water',NULL,49,49,56,49,61,66,330);
INSERT INTO pokemon VALUES (457,'Lumineon','Water',NULL,69,69,76,69,86,91,460);

-- 73 Pokemon Inserted

INSERT INTO strong_against VALUES ('Fighting','Normal, Rock, Steel, Ice, Dark');
INSERT INTO strong_against VALUES ('Normal','');
INSERT INTO strong_against VALUES ('Flying','Fighting, Bug, Grass');
INSERT INTO strong_against VALUES ('Poison','Grass, Fairy');
INSERT INTO strong_against VALUES ('Ground','Poison, Rock, Steel, Fire, Electric');
INSERT INTO strong_against VALUES ('Rock','Flying, Bug, Fire, Ice');
INSERT INTO strong_against VALUES ('Bug','Grass, Psychic, Dark');
INSERT INTO strong_against VALUES ('Ghost','Ghost, Psychic');
INSERT INTO strong_against VALUES ('Steel','Rock, Ice, Fairy');
INSERT INTO strong_against VALUES ('Fire','Bug, Steel, Grass, Ice');
INSERT INTO strong_against VALUES ('Water','Ground, Rock, Fire');
INSERT INTO strong_against VALUES ('Grass','Ground, Rock, Water');
INSERT INTO strong_against VALUES ('Electric','Flying, Water');
INSERT INTO strong_against VALUES ('Psychic','Fighting, Poison');
INSERT INTO strong_against VALUES ('Ice','Flying, Ground, Grass, Dragon');
INSERT INTO strong_against VALUES ('Dragon','Dragon');
INSERT INTO strong_against VALUES ('Fairy','Fighting, Dragon, Dark');
INSERT INTO strong_against VALUES ('Dark','Ghost, Psychic');

INSERT INTO weak_against VALUES ('Fighting','Flying, Poison, Psychic, Bug, Ghost, Fairy');
INSERT INTO weak_against VALUES ('Normal','Rock, Ghost, Steel');
INSERT INTO weak_against VALUES ('Flying','Rock, Steel, Electric');
INSERT INTO weak_against VALUES ('Poison','Poison, Ground, Rock, Ghost, Steel');
INSERT INTO weak_against VALUES ('Ground','Flying, Bug, Grass');
INSERT INTO weak_against VALUES ('Rock','Fighting, Ground, Steel');
INSERT INTO weak_against VALUES ('Bug','Fighting, Flying, Poison, Ghost, Steel, Fire, Fairy');
INSERT INTO weak_against VALUES ('Ghost','Normal, Dark');
INSERT INTO weak_against VALUES ('Steel','Steel, Fire, Water, Electric');
INSERT INTO weak_against VALUES ('Fire','Rock, Fire, Water, Dragon');
INSERT INTO weak_against VALUES ('Water','Water, Grass, Dragon');
INSERT INTO weak_against VALUES ('Grass','Flying, Poison, Bug, Steel, Fire, Grass, Dragon');
INSERT INTO weak_against VALUES ('Electric','Ground, Grass, Electric, Dragon');
INSERT INTO weak_against VALUES ('Psychic','Steel, Psychic, Dark');
INSERT INTO weak_against VALUES ('Ice','Steel, Fire, Water, Ice');
INSERT INTO weak_against VALUES ('Dragon','Steel, Fairy');
INSERT INTO weak_against VALUES ('Fairy','Poison, Steel, Fire');
INSERT INTO weak_against VALUES ('Dark','Fighting, Dark, Fairy');


INSERT INTO resistant VALUES ('Fighting','Rock, Bug, Dark');
INSERT INTO resistant VALUES ('Normal','Ghost');
INSERT INTO resistant VALUES ('Flying','Normal, Rock, Steel, Ice, Dark');
INSERT INTO resistant VALUES ('Poison','Fighting, Poison, Grass, Fairy');
INSERT INTO resistant VALUES ('Ground','Poison, Rock, Electric');
INSERT INTO resistant VALUES ('Rock','Normal, Flying, Poison, Fire');
INSERT INTO resistant VALUES ('Bug','Fighting, Ground, Grass');
INSERT INTO resistant VALUES ('Ghost','Normal, Fighting, Poison, Bug');
INSERT INTO resistant VALUES ('Steel','Normal, Flying, Poison, Rock, Bug, Steel, Grass, Psychic, Ice, Dragon, Fairy');
INSERT INTO resistant VALUES ('Fire','Bug, Steel, Fire, Grass, Ice');
INSERT INTO resistant VALUES ('Water','Steel, Fire, Water, Ice');
INSERT INTO resistant VALUES ('Grass','Ground, Water, Grass, Electric');
INSERT INTO resistant VALUES ('Electric','Flying, Steel, Electric');
INSERT INTO resistant VALUES ('Psychic','Fighting, Psychic');
INSERT INTO resistant VALUES ('Ice','Ice');
INSERT INTO resistant VALUES ('Dragon','Fire, Water, Grass, Electric');
INSERT INTO resistant VALUES ('Fairy','Fighting, Bug, Dragon, Dark');
INSERT INTO resistant VALUES ('Dark','Ghost, Psychic, Dark');

INSERT INTO vulnerable VALUES ('Fighting','Flying, Psychic, Fairy');
INSERT INTO vulnerable VALUES ('Normal','Fighting');
INSERT INTO vulnerable VALUES ('Flying','Rock, Electric, Ice');
INSERT INTO vulnerable VALUES ('Poison','Ground, Psychic');
INSERT INTO vulnerable VALUES ('Ground','Water, Grass, Ice');
INSERT INTO vulnerable VALUES ('Rock','Fighting, Ground, Steel, Water, Grass');
INSERT INTO vulnerable VALUES ('Bug','Flying, Rock, Fire');
INSERT INTO vulnerable VALUES ('Ghost','Ghost, Dark');
INSERT INTO vulnerable VALUES ('Steel','Fighting, Ground, Fire');
INSERT INTO vulnerable VALUES ('Fire','Ground, Rock, Water');
INSERT INTO vulnerable VALUES ('Water','Grass, Electric');
INSERT INTO vulnerable VALUES ('Grass','Flying, Poison, Bug, Fire, Ice');
INSERT INTO vulnerable VALUES ('Electric','Ground');
INSERT INTO vulnerable VALUES ('Psychic','Bug, Ghost, Dark');
INSERT INTO vulnerable VALUES ('Ice','Fighting, Rock, Steel, Fire');
INSERT INTO vulnerable VALUES ('Dragon','Ice, Dragon, Fairy');
INSERT INTO vulnerable VALUES ('Fairy','Poison, Steel');
INSERT INTO vulnerable VALUES ('Dark','Fighting, Bug, Fairy');

