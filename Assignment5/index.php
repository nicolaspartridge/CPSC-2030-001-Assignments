<!DOCTYPE html>
<html>
<head>
	<title>Assignment 5</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
</head>

<?php
	$servername = "localhost";
	$username = "CPSC2030";
	$password = "CPSC2030";
	$dbName = "pokedex";

	// Create connection
	$conn = mysqli_connect($servername, $username, $password, $dbName);

	// Check connection
	if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
	}
	

	$page_type = isset($_GET['type']) ? $_GET['type'] : '';
	$showall = isset($_GET['showall']) ? $_GET['showall'] : 1;

	if($showall != 1) {
		$titleT = $page_type;
	} else {
		$titleT = "All";
	}

	
?>

<body>
	<div class="container">

		<h1 class="text-center">Showing <?php echo $titleT; ?> Type(s)</h1>
		<h4><a href="index.php">Reset</a></h4>

		<hr>

		<div class="row text-center">
		<?php
			$sql = "CALL pokedexGetByType('".$page_type."', '".$showall."')";
			$result = mysqli_query($conn, $sql);
			$resultCheck = mysqli_num_rows($result);

			if ($resultCheck > 0) {
				while ($row = mysqli_fetch_assoc($result)) { 

					$num = $row['num'];
					$name = $row['name'];
					$type1 = $row['type'];
					$type2 = '';
					if ($row['type2'] != NULL) {
						$type2 = $row['type2'];
					} 


					?>
					<div class="col-sm-3 text-center" style="margin-bottom: 50px;">
						<div class="card w-100" style="width: 18rem;">
						  <!-- <img class="card-img-top" src="cardtop-pic.svg" alt="Card image cap"> -->
						  <div class="card-img-top text-center"><span style="font-size: 24px; font-weight: bold;"><?php echo $num  ?></span></div>
						  <div class="card-body">
						    <h5 class="card-title"><?php echo $name ?></h5>
						    <p class="card-text"><?php echo "<a href='index.php?type=". $type1 . "&showall=0'>" . $type1 . "</a>" . " " . "<a href='index.php?type=". $type2 . "&showall=0'>" . $type2 . "</a>" ?></p>
						    <a href='single.php?id=<?php echo $num . "&name=" . $name ?>' class="btn btn-primary">More</a>
						  </div>
						</div>
					</div>
					
				<?php
				 }
			}
		?>
		</div>
	</div>
	


	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
</body>
</html>