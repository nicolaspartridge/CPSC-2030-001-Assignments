<!DOCTYPE html>
<html>
<head>
	<title>Assignment 5</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
</head>

<?php
	$servername = "localhost";
	$username = "CPSC2030";
	$password = "CPSC2030";
	$dbName = "pokedex";

	// Create connection
	$conn = mysqli_connect($servername, $username, $password, $dbName);

	// Check connection
	if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
	}

	$name = isset($_GET['name']) ? $_GET['name'] : '';
	$num = isset($_GET['id']) ? $_GET['id'] : -1;

	$strength = Array('','');
	$weakness = Array('','');
	$vulnerable = Array('','');
	$resistant = Array('','');
	
	$count = 0;
?>

<body>

	<?php
		$sql = "CALL pokedexGetByNum('".$num."')";
		$result = mysqli_query($conn, $sql);
		$resultCheck = mysqli_num_rows($result);

		if ($resultCheck > 0) {
			while ($row = mysqli_fetch_assoc($result)) { 

				$hp = $row['hp'];
				$atk = $row['atk'];
				$def = $row['def'];
				$sAT = $row['sAT'];
				$sDf = $row['sDf'];
				$spd = $row['spd'];
				$BST = $row['BST'];

				$type1 = $row['type'];
				$type2 = '';
				if ($row['type2'] != NULL) {
					$type2 = $row['type2'];
				} 
			}

			// Free result set
            mysqli_free_result($result);   
			mysqli_next_result($conn); 

		}

		$result = mysqli_query($conn, "CALL pokedexGetAttributes('".$type1."','".$type2."')");
		$resultCheck = mysqli_num_rows($result);

		if ($resultCheck > 0) {
			$rows = array();
			while($row = $result->fetch_assoc()) {
        		$rows[] = $row;
    		}

			foreach ($rows as $row) {
			 	
				$strength[$count] = $row['strong_against'];
				$weakness[$count] = $row['weak_against'];
				$vulnerable[$count] = $row['vulnerable'];
				$resistant[$count] = $row['resistant'];

				$count++;
			}

			mysqli_free_result($result);   
			mysqli_next_result($conn); 
		}

		$strength[0] = implode(',',array_unique(explode(',', $strength[0]." ".$strength[1])));
		$weakness[0] =  implode(',',array_unique(explode(',', $weakness[0]." ".$weakness[1])));
		$vulnerable[0] =  implode(',',array_unique(explode(',', $vulnerable[0]." ".$vulnerable[1])));
		$resistant[0] =  implode(',',array_unique(explode(',', $resistant[0]." ".$resistant[1])));

		
	?>

	<div class="container">

		<h1 class="text-center" style="margin-top: 50px;"><?php echo $name; ?></h1>
		<h4><a href="index.php">Reset</a></h4>

		<hr>

		<div class="row">
			<div class="col-sm-12" style="margin-top: 50px;">
				<div class="card w-100 jumbotron" style="width: 18rem;padding: 1rem 1rem;">
				  <!-- <img class="card-img-top" src="cardtop-pic.svg" alt="Card image cap"> -->
				  <div class="card-img-top">
					  	<div class="row">
					  		<div class="col-sm-2">
					  		<span style="font-size: 24px; font-weight: bold; background: #555; border-radius: 5px; padding: 5px; color: white;">#<?php echo $num  ?></span>
					  	</div>

					  	<div class="col-sm-8">
					  		 <p class="card-title"><span style="font-size: 24px; font-weight: bold;"><?php echo $name ?></span> is a(n) <?php echo "<a href='index.php?type=". $type1 . "&showall=0'>" . $type1 . "</a>" . " " . "<a href='index.php?type=". $type2 . "&showall=0'>" . $type2 . "</a>" ?> type pokemon.</p>
					  	</div>

						<div class="col-sm-2">
							<p style="font-weight: bold">BST: <?php echo $BST ?></p>
						</div>
				  	</div>
				  	
				  	 <hr>
				  </div>
				  <div class="card-body row text-center">

				  	<p class="col-md-2 col-sm-2"><span style="font-weight: bold;">HP:</span> <?php echo $hp ?></p>
				  	<p class="col-md-2 col-sm-2"><span style="font-weight: bold;">ATK:</span> <?php echo $atk ?></p>
				  	<p class="col-md-2 col-sm-2"><span style="font-weight: bold;">DEF:</span> <?php echo $def ?></p>
				  	<p class="col-md-2 col-sm-2"><span style="font-weight: bold;">sAT:</span> <?php echo $sAT ?></p>
				  	<p class="col-md-2 col-sm-2"><span style="font-weight: bold;">sDF:</span> <?php echo $sDf ?></p>
				  	<p class="col-md-2 col-sm-2"><span style="font-weight: bold;">SPD:</span> <?php echo $spd ?></p>
				   
				    <p class="card-text">
				    	<div class="col-sm-12">
				    		<?php echo $name ?> is strong against <span style="font-weight: bold;"><?php echo $strength[0] ?></span> type(s), weak against <span style="font-weight: bold;"><?php echo $weakness[0] ?></span> types(s), resistant to <span style="font-weight: bold;"><?php echo $resistant[0] ?></span> types(s) and vulnerable to <span style="font-weight: bold;"><?php echo $vulnerable[0] ?></span> type(s)
				    	</div>
				    </p>
				  </div>
				</div>
			</div>
		</div>
	</div>


	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
</body>
</html>