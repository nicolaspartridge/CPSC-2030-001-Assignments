<?php
	include 'server.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>Assignment 9</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="style.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
  	<script src="chat.js"></script>
</head>
<body>
	<script type="text/javascript">
		$(document).ready(function() {
			let chatCount = 10;

			function updateChat() {
				chatCount++;
				$("#chatlogs").load("update.php", {
					newChatCount: chatCount
				});
			}

			setInterval(function() {
				chatCount++;
				$("#chatlogs").load("update.php", {
					newChatCount: chatCount
				});
			}, 1000);

			$('button').click(function() {
				let msg = $('#message').val();
				let name = $('#enterName').val();

				$.ajax({
					url: "sendMsg.php",
					type: "post",
					async: true,
					data: {
						"done": 1,
						"username": name,
						"message": msg
					},
					success: function(data) {
						console.log("data sent");
					}
				})

				updateChat();
			});


			// $('button').click(function() {
			// 	let msg = $('#message').val();
			// 	let name = $('#enterName').val();
			// 	$.post('sendMsg.php', {
			// 		message: msg,
			// 		name: name
			// 	});
			// });
		});
	</script>

	<div class="container">
		<div class="row">
			<h2 class="text-center col-8">AJAX Chatroom</h2>
			
			<div id="chatlogs" class="col-12">
				<?php
					$sql = "SELECT * FROM chats ORDER BY id ASC LIMIT 10";
					$result = mysqli_query($conn, $sql);
					if (mysqli_num_rows($result) > 0){
						while ($row = mysqli_fetch_assoc($result)) {
							echo "<p><strong>" . $row['ChatUserName'] . "</strong><br>" . $row['ChatText'] . "</p>";
						}
					} else {
						echo "There are no chat logs.";
					}
				?>
			</div>
		</div>

		<div class="row text-center">
			<form id="send-message-area" name="chatForm" class="col-12">
			<p class="col-12 user">Enter your Chatname: <input id="enterName" type="text" name="username" /></p>
			<p>Your message: </p>
			<textarea id="message" maxlength = '255' class="col-12"></textarea>
			
			</form>
			<button class="btn btn-primary col-12">Send</button>
			

			
		</div>
	</div>
</body>
</html>