$('.menu-toggle').click(function(){
	$('.site-nav').toggleClass('site-nav--open', 450);
	$('header').toggleClass('header--open', 450);
	$(this).toggleClass('open');
	$('.logo').toggleClass('logo-open');
	$('.content').toggleClass('hide');
	console.log('hide');
})

$('.home-button').click(function(){
	$('.site-nav').removeClass('site-nav--open', 450);
	$('header').removeClass('header--open', 450);
	$('.menu-toggle').removeClass('open');
	$('.logo').removeClass('logo-open');
})

$('.about-button').click(function(){
	$('.site-nav').removeClass('site-nav--open', 450);
	$('header').removeClass('header--open', 450);
	$('.menu-toggle').removeClass('open');
	$('.logo').removeClass('logo-open');
})

$('.projects-button').click(function(){
	$('.site-nav').removeClass('site-nav--open', 450);
	$('header').removeClass('header--open', 450);
	$('.menu-toggle').removeClass('open');
	$('.logo').removeClass('logo-open');
})

$( window ).resize(function() {
	$('canvas').css('height', '100%');
});

$( document ).ready(function() {
    console.log( "ready!" );
});
