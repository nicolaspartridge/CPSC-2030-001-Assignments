<?php
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Services</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

	<!-- Linking my Less Stylesheet -->
    <link rel="stylesheet" type="text/less" href="styles.less?ts=<?=filemtime('styles.less')?>">
    <!-- The Less CDN  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>

</head>

<?php

	require 'vendor/autoload.php';
	$loader = new Twig_Loader_Filesystem('views');
	$twig = new Twig_Environment($loader);

	$lexer = new Twig_Lexer($twig, array(
		'tag_block' => array('{','}'),
		'tag_variable' => array('{{','}}'),
	));

	$twig->setLexer($lexer);

?>

<body>
	<canvas id="canvas"></canvas>
	<div class="container-all">

		<!-- Twig template for the header rendered -->
		<?php 
			echo $twig->render('header.html', array(
				'links' => array(
					array('name' => 'Home', 'link' => 'index'),
					array('name' => 'About', 'link' => 'about'),
					array('name' => 'Services', 'link' => 'services'),
					array('name' => 'Projects', 'link' => 'projects'),
					array('name' => 'Contact', 'link' => 'contact'),
				),
				'name' => 'Sample Name'
			));
		?>
		
		<!-- Code for content begins -->

		<!-- Twig temaplate for page title rendered -->
		<?php 
			echo $twig->render('title.html', array(
				'title' => 'What we offer.'
			));
		?>

	
		<div class="content-services content-container">
			<div class="card">
				<h1>The Reverse Engineer</h1>
				<p>We offer exclusive, fully confidential if necesssary, reverse engineering of <strong>anything</strong>. From this, we can either determine what makes a product so good, or how a product is made. This includes, but is not limited to mechanical and biological devices/substances. Reach out to us for a consultation <a href="contact.php">here</a>.</p>
				<br>
				<p><small>*In order to qualify for a consultation, make sure you have registered an account on this website.</small></p>
			</div>
		</div>


		<!-- Code for content ends -->


		
		<?php
			require "footer.php";
		?>
		

    </div>
	<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="particle.js"></script>
    <script src="main.js?ts=<?=filemtime('main.js')?>"></script>
</body>
</html>