<?php 

$servername = "localhost";
$dbUsername = "";
$dbPassword = "";
$dbName = "project";

$conn = mysqli_connect($servername, $dbUsername, $dbPassword, $dbName);

if (!$conn) {
	die("Connection failed: ". mysqli_connect_error());
}

