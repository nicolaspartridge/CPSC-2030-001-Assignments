CPSC 2030 Project Technical Checklist:

* SQL Setup scripts: located in the 'db.sql' file 
* Contains a log-on system: The main page has a sign up and log on system built into it.
* Contains at lest 6 pages:  My website contains a main page, a client/user dashboard page, an about page, a services page, a projects page, and a contact page, totaling 6 pages.
* Use at least 3 twig templates:  I use twig templates countless times across my website to load headers and other parts of pages, I use the 3 main templates about.html, header.html and title.html which are all located in the 'views' folder.
* Use JQuery to implement no trivial features:
	* The particle background across my entire website is very non-trivial and utilizes JQuery (This took a long time to do and was done completely from scratch) If you click you also generate more particles at random sizes and colours. The code for this is located in particle.js.
	* I use JQuery exclusivley for my collapsable navbar and hamburger menu. Code located in main.js.
* Contains at least one page using AJAX that pulls data from back end:  On my user dashboard it pulls data seamlessly once you log in, the contact form also auto-fills with your email if you are logged in.
* A small PHP library to generate common components: I use a small PHP library to generate the footers across my entire website.
* Each page includes navigation to the entire website, located in the navbar, as well as a footer.
* I utilize LESS stylesheets to help style my website.
* The page is responsive and mobile friendly.