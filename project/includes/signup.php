<?php
//Sign user up

//Check if they accessed the page by submitting or entering a url, we only want submit
if (isset($_POST['signup-submit'])) {
	require 'database.php';

	//Grab variables from post from form entered by user
	$username = $_POST['uid'];
	$email = $_POST['mail'];
	$password = $_POST['pwd'];
	$passwordRepeat = $_POST['pwd-repeat'];

	//Check if any of the fields were left blank, if they were return to sign up with error message
	if (empty($username) || empty($email) || empty($password) || empty($passwordRepeat)) {
		header("Location: ../index.php?error=emptyfields&uid=".$username."&mail=".$email);
		exit();
	}
	//Check if email is invalid and username is invalid, if one is invalid return to sign up with error message
	else if (!filter_var($email, FILTER_VALIDATE_EMAIL) && !preg_match("/^[a-zA-Z0-9]*$/", $username)) {
		header("Location: ../index.php?error=invalidmailuid");
		exit();
	}
	//Check if email is valid, if invalid return to sign up with error message
	else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		header("Location: ../index.php?error=invalidmail&uid=".$email);
		exit();
	}
	//Check if username is valid, if invalid return to sign up with error message
	else if (!preg_match("/^[a-zA-Z0-9]*$/", $username)) {
		header("Location: ../index.php?error=invaliduid&uid=".$username);
		exit();
	}
	//Check if passwords match, if invalid return to sign up with error message
	else if ($password !== $passwordRepeat) {
		header("Location: ../index.php?error=passwordcheck&uid=".$username."&mail=".$email);
		exit();
	}
	else {
		//If all fields valid 
		$sql = "SELECT uidUsers FROM users WHERE uidUsers=?";
		$statement = mysqli_stmt_init($conn);
		//User prepared statement to check for db error
		if (!mysqli_stmt_prepare($statement, $sql)) {
			header("Location: ../index.php?error=sqlerror");
			exit();
		}
		else {
			//Use prepared statement to check is username is taken already, if taken return to sign up with error message
			mysqli_stmt_bind_param($statement, "s", $username);
			mysqli_stmt_execute($statement);
			mysqli_stmt_store_result($statement);
			$resultCheck = mysqli_stmt_num_rows($statement);
			if ($resultCheck > 0) {
				header("Location: ../index.php?error=usertaken&mail=".$email);
				exit();
			}
			else {
				//Check for db error
				$sql = "INSERT INTO users (uidUsers, emailUsers, pwdUsers) VALUES (?,?,?)";
				$statement = mysqli_stmt_init($conn);
				if (!mysqli_stmt_prepare($statement, $sql)) {
					header("Location: ../index.php?error=sqlerror");
					exit();
				}
				else {
					//Hash password
					$hashPassword = password_hash($password, PASSWORD_DEFAULT);
					//Add user to db, display sign up success
					mysqli_stmt_bind_param($statement, "sss", $username, $email, $hashPassword);
					mysqli_stmt_execute($statement);
					header("Location: ../index.php?signup=success");
					exit();
				}
			}
		}
	}
	mysqli_stmt_close($statement);
}
else {
	header("Location: ../index.php");
	exit();
}