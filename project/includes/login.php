<?php
// Log user in

//Check if they accessed the page by submitting or entering a url, we only want submit
if (isset($_POST['login-submit'])) {
	require 'database.php';

	//Grain info from log in form
	$mailuid = $_POST['mailuid'];
	$password = $_POST['pwd'];

	//Check if any fields are empty
	if (empty($mailuid) || empty($password)) {
		header("Location: ../index.php?error=emptyfields");
		exit();
	}
	else {
		//Check db for username or email entered using prepared statements
		$sql = "SELECT * from users WHERE uidUsers=? OR emailUsers=?;";
		$statement = mysqli_stmt_init($conn);
		//Check for db error
		if (!mysqli_stmt_prepare($statement, $sql)) {
			header("Location: ../index.php?error=sqlerror");
			exit();
		}
		else {
			mysqli_stmt_bind_param($statement, "ss", $mailuid, $mailuid);
			mysqli_stmt_execute($statement);
			$results = mysqli_stmt_get_result($statement);
			//Check is passwords match
			if ($row = mysqli_fetch_assoc($results)) {
				$passwordCheck = password_verify($password, $row['pwdUsers']);
				if (!$passwordCheck) {
					header("Location: ../index.php?error=wrongpassword");
					exit();
				}
				//Log in user to website
				else if ($passwordCheck) {
					//Start sesstion and create session variables
					session_start();
					$_SESSION['userId'] = $row['idUsers'];
					$_SESSION['userUid'] = $row['uidUsers'];
					$_SESSION['userEmail'] = $row['emailUsers'];

					header("Location: ../index.php?login=success");
					exit();
				}
				else {
					header("Location: ../index.php?error=wrongpassword");
					exit();
				}
			}
			else {
				header("Location: ../index.php?error=nouser");
				exit();
			}
		}
	}
}
else {
	header("Location: ../index.php");
	exit();
}