<?php
//Starts session
session_start();
//Clears session variables
session_unset();
//Kills the session adn sends user home
session_destroy();
header("Location: ../index.php");