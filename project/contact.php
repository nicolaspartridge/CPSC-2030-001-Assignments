<?php
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Contact</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

	<!-- Linking my Less Stylesheet -->
    <link rel="stylesheet" type="text/less" href="styles.less?ts=<?=filemtime('styles.less')?>">
    <!-- The Less CDN  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
    <link rel="stylesheet" href="animations.css">

</head>

<?php

	require 'vendor/autoload.php';
	$loader = new Twig_Loader_Filesystem('views');
	$twig = new Twig_Environment($loader);

	$lexer = new Twig_Lexer($twig, array(
		'tag_block' => array('{','}'),
		'tag_variable' => array('{{','}}'),
	));

	$twig->setLexer($lexer);

?>

<body>
	<canvas id="canvas"></canvas>
	<div class="container-all">

		<!-- Twig template for the header rendered -->
		<?php 
			echo $twig->render('header.html', array(
				'links' => array(
					array('name' => 'Home', 'link' => 'index'),
					array('name' => 'About', 'link' => 'about'),
					array('name' => 'Services', 'link' => 'services'),
					array('name' => 'Projects', 'link' => 'projects'),
					array('name' => 'Contact', 'link' => 'contact'),
				),
				'name' => 'Sample Name'
			));
		?>
		
		<!-- Code for content begins -->

		<div class="">
			
			<!-- Twig temaplate for page title rendered -->
			<?php 
				echo $twig->render('title.html', array(
					'title' => 'Work with us.'
				));
			?>

			<div class="container">
				<div class="contact-form">
					<form>

						<div class="half-row">
							<input type="text" name="firstname" placeholder="First Name" class="">
							<input type="text" name="lastname" placeholder="Last Name" class="">
						</div>

						<div class="full-row">
							<?php 
							if(isset($_SESSION['userEmail'])) {
								echo '
									<input type="text" name="email" placeholder="Email Address" class="" value="'.$_SESSION['userEmail'].'"> <br>
								';
							}
							else {
								echo '
									<input type="text" name="email" placeholder="Email Address" class=""> <br>
								';
							}
							?>
							
						</div>

						<div class="half-row">
							<input type="text" name="country" placeholder="Country" class="">
							<input type="text" name="phone" placeholder="Phone Number" class="">
						</div>

						<div class="full-row">
							<textarea name="message" placeholder="How can we help?" class="" rows="5"></textarea>
						</div>

						<div class="full-row">
							<div class="col-md-2"></div>
							<button type="button" class="btn">SUBMIT</button>
						</div>

					</form>
				</div>
			</div>

		</div>


		<!-- Code for content ends -->
	
		
		<?php
			require "footer.php";
		?>

    </div>
	<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="particle.js"></script>
    <script src="main.js?ts=<?=filemtime('main.js')?>"></script>
</body>
</html>