<?php
	session_start();
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Nicolas Partridge</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

	<!-- Linking my Less Stylesheet -->
    <link rel="stylesheet" type="text/less" href="styles.less?ts=<?=filemtime('styles.less')?>">
    <!-- The Less CDN  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
    <link rel="stylesheet" href="animations.css">

  </head>
	<?php

	require 'vendor/autoload.php';
	$loader = new Twig_Loader_Filesystem('views');
	$twig = new Twig_Environment($loader);

	$lexer = new Twig_Lexer($twig, array(
		'tag_block' => array('{','}'),
		'tag_variable' => array('{{','}}'),
	));

	$twig->setLexer($lexer);

	?>


  <body>
  	<canvas id="canvas"></canvas>
    <div class="container-all">

		<!-- Twig template for the header rendered -->
		<?php 
			echo $twig->render('header.html', array(
				'links' => array(
					array('name' => 'Home', 'link' => 'index'),
					array('name' => 'About', 'link' => 'about'),
					array('name' => 'Services', 'link' => 'services'),
					array('name' => 'Projects', 'link' => 'projects'),
					array('name' => 'Contact', 'link' => 'contact'),
				),
				'name' => 'Sample Name'
			));
		?>
		
		<!-- Code for content begins -->

		<!-- Twig temaplate for page title rendered -->
	
			<?php 
			echo $twig->render('title.html', array(
				'title' => 'Engineered Exellence.'
			));
		
		?>
	
		
		<div class="content text-center">
			<?php
				if(isset($_SESSION['userId'])) {
						echo '
						<div class="loggedin col-12">
						<div class="row">
							<div class="left col-12 col-md-6">
								<div class="welcome">
									<div class="row">
										<h1 class="col-12">Welcome,<br> '.$_SESSION['userUid'].'</h1>
										<h1 id="user"></h1>
									</div>
								</div>
								</div>

								<div class="right col-12 col-md-6">

								<div class="welcome">
									<div class="row">
										<h3 class="col-12">Account Details</h3>
										<p id="userEmail" class="col-12 col-md-6"><strong>Email:</strong> '.$_SESSION['userEmail'].'</p>
										<p id="userName" class="col-12 col-md-6"><strong>Username:</strong> '.$_SESSION['userUid'].' </p>
									</div>
								</div>

								<form action="includes/logout.php" method="post">
									<button class="btn col-12 col-md-6" type="submit" name="logout-submit">Logout</button>
								</form>
								</div>
							</div>
						
						</div>
						';
					}
					else  {
						echo '
						<div class="loggedout col-12">
						<h1>Sign in</h1>
						<form action="includes/login.php" method="post">
							<input class="col-3" type="text" name="mailuid" placeholder="Username/Email">
							<input class="col-3" type="password" name="pwd" placeholder="Password"><br>
							<button class="btn col-3" type="submit" name="login-submit">Login</button>
						</form> 

						<h1>Sign up</h1>
						<form action="includes/signup.php" method="post">
							<input class="col-3" type="text" name="uid" placeholder="Username">
							<input class="col-3" type="text" name="mail" placeholder="Email"><br>
							<input class="col-3" type="password" name="pwd" placeholder="Password">
							<input class="col-3" type="password" name="pwd-repeat" placeholder="Repeat Password"><br>
							<button class="btn col-3" type="submit" name="signup-submit">Signup</button>
						</form>
						
						</div>
						
						';
					}

				?>
			

			
		</div>

		<!-- Code for content ends -->

		
		<!-- Footer rendered from php library -->
		
		
		<?php
			require "footer.php";
		?>

    </div>
	
    <script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="particle.js"></script>
    <script src="main.js?ts=<?=filemtime('main.js')?>"></script>
  </body>
</html>
