<?php
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Projects</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

	<!-- Linking my Less Stylesheet -->
    <link rel="stylesheet" type="text/less" href="styles.less?ts=<?=filemtime('styles.less')?>">
    <!-- The Less CDN  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>

</head>

<?php

	require 'vendor/autoload.php';
	$loader = new Twig_Loader_Filesystem('views');
	$twig = new Twig_Environment($loader);

	$lexer = new Twig_Lexer($twig, array(
		'tag_block' => array('{','}'),
		'tag_variable' => array('{{','}}'),
	));

	$twig->setLexer($lexer);

?>

<body>
	<canvas id="canvas"></canvas>
	<div class="container-all">

		<!-- Twig template for the header rendered -->
		<?php 
			echo $twig->render('header.html', array(
				'links' => array(
					array('name' => 'Home', 'link' => 'index'),
					array('name' => 'About', 'link' => 'about'),
					array('name' => 'Services', 'link' => 'services'),
					array('name' => 'Projects', 'link' => 'projects'),
					array('name' => 'Contact', 'link' => 'contact'),
				),
				'name' => 'Sample Name'
			));
		?>
		
		<!-- Code for content begins -->

		<!-- Twig temaplate for page title rendered -->
		<?php 
			echo $twig->render('title.html', array(
				'title' => "What we've done."
			));
		?>

		<!-- Code for content ends -->
		<div class="content-projects content-container">
			<div class="row">

				<div class="card col-12">
					<h3>Mechanical</h3>
					<p>We specialize in mechanical reverse engineering, our CEO and project lead, Michael Jennings, has over 20 years of mechanical engineering experience. What we do is we take apart whatever device, mechanical or technologcal, and reverse engineer it, providing you with details on how it works, how its make and how it can be improved or optimized.</p>
				</div>
			
				<div class="card col-12">
					<h3>Biological</h3>
					<p>Recently we have opened to firm up to biological reverse engineering, this includes the reverse egineering of biological warfare related weapons, all the way to tracking and history of biomarkers.<br><small>*Our bioengineering is not limited to human biology.</small></p>
				</div>

			</div>
			
		</div>
		
		<?php
			require "footer.php";
		?>
		

    </div>
	<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="particle.js"></script>
    <script src="main.js?ts=<?=filemtime('main.js')?>"></script>
</body>
</html>