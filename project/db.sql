-- Creates database
CREATE DATABASE project;

-- Creates table
CREATE TABLE users (
	idUsers INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
	uidUsers TINYTEXT NOT NULL,
	emailUsers TINYTEXT NOT NULL,
	pwdUsers TEXT NOT NULL
);
