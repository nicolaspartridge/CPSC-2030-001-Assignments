
-- Find all pokemon who has a BST between 200 to 500 who si weak against water types
SELECT * FROM pokemon WHERE BST >= 200 AND BST <= 500 AND type='STEEL' OR type='FIRE' OR type='WATER' OR type='ICE';

-- Find the bottom ten pokemon ranked by speed
SELECT name FROM pokemon order by spd asc limit 10;
 