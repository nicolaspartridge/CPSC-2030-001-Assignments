<!DOCTYPE html>
<html>
<head>
	<title>Assignment 6</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- <link rel="stylesheet/less" type="text/css" media="screen" href="style.less" /> -->
   <!--  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous"> -->
	<link rel="stylesheet/less" type="text/css" media="screen" href="style.css" />
    
    <?php 
	

	require 'lessc.inc.php';
	$less = new lessc();

    // Compile the less file to css
    $less->compileFile("style.less", 'style.css');
    

	require 'vendor/autoload.php';
	$loader = new Twig_Loader_Filesystem('views');
	$twig = new Twig_Environment($loader);

	$lexer = new Twig_Lexer($twig, array(
		'tag_block' => array('{','}'),
		'tag_variable' => array('{{','}}'),
	));

	$twig->setLexer($lexer);
?>

</head>

<?php
	$servername = "localhost";
	$username = "";
	$password = "";
	$dbName = "pokedex";

	

	// Create connection
	$conn = mysqli_connect($servername, $username, $password, $dbName);

	// Check connection
	if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
	}
	

	$page_type = isset($_GET['type']) ? $_GET['type'] : '';
	$showall = isset($_GET['showall']) ? $_GET['showall'] : 1;

	if($showall != 1) {
		$titleT = $page_type;
	} else {
		$titleT = "All";
	}

	
?>

<body>

	<?php
		$sql = "CALL pokedexGetAll()";
		$result = mysqli_query($conn, $sql);
		$resultCheck = mysqli_num_rows($result);
		$pokemon = array();
		$favourites = array();


		

		function addCard($conn) {
			$add_num = isset($_GET['id']) ? $_GET['id'] : '';
			$add_name = isset($_GET['name']) ? $_GET['name'] : '';
			$add_type1 = isset($_GET['type1']) ? $_GET['type1'] : '';
			$add_type2 = isset($_GET['type2']) ? $_GET['type2'] : '';
			
		}

		

    	if (isset($_POST['removeP'])) {
    		$conn->next_result();

        	$info = explode(",",$_POST['removeP']);

        	$sql = "UPDATE pokemon SET fav='0' WHERE num=$info[0]";
			if (mysqli_query($conn, $sql)) {
			    echo "Record updated successfully";
			} else {
			    echo "Error updating record: " . mysqli_error($conn);
			}
       		// Do the database update code to set Accept
    		$conn->next_result();
    		$num_of_favs--;
    		header('Location: '.$_SERVER['REQUEST_URI']);
    	}


		if ($resultCheck > 0) {
			while ($row = mysqli_fetch_assoc($result)) { 

				$num = $row['num'];
				$name = $row['name'];
				$type1 = $row['type'];
				$type2 = '';
				$isFav = $row['fav'];
				if ($row['type2'] != NULL) {
					$type2 = $row['type2'];
				} 
				array_push($pokemon, array($num, $name, $type1, $type2));

				if($isFav):
					array_push($favourites, array($num, $name, $type1, $type2));
				endif;
			}

			$num_of_favs = sizeof($favourites);
			
		}

		if (isset($_POST['updateP'])) {
			if($num_of_favs < 7) {
				$conn->next_result();

	        	$info = explode(",",$_POST['updateP']);

	        	$sql = "UPDATE pokemon SET fav='1' WHERE num=$info[0]";
				if (mysqli_query($conn, $sql)) {
				    echo "Record updated successfully";
				} else {
				    echo "Error updating record: " . mysqli_error($conn);
				}
	       		// Do the database update code to set Accept
	    		$conn->next_result();
	    		$num_of_favs++;
	    		header('Location: '.$_SERVER['REQUEST_URI']);

			}
    		
    	}


	?>					

	<header>
		<h1>
			<?php 
			echo $twig->render('header.html', array(
			'title' => 'Assignment 6 Twig Templates'
			));
			?>
		</h1>
	</header>

	<div class="container">
		
		<div class="side-bar">
			<h3>Mega Pokemon</h3>
			<ul>
				<?php 
				foreach($pokemon as $poke):
					if(strpos($poke[1], 'Mega') !== false):

					echo $twig->render('sidebar.html', array(
						'pokemon' => array(
						 	array('num' => $poke[0], 'name' => $poke[1], 'type1' => $poke[2], 'type2' => $poke[3])
						)
					));

					endif;
				endforeach;
				?>
			</ul>
		</div>

		<div class="card-section">
			<?php 
				foreach($favourites as $fav):
				echo $twig->render('card.html', array(
					'pokemon' => array(
					 	array('num' => $fav[0], 'name' => $fav[1], 'type1' => $fav[2], 'type2' => $fav[3])
					)
				));
				endforeach;
			?>
		</div>

		<div class="table">
			
				<form method="post" action="">
					
					<?php 
					foreach($pokemon as $poke):
					echo $twig->render('table.html', array(
						'pokemon' => array(
						 	array('num' => $poke[0], 'name' => $poke[1], 'type1' => $poke[2], 'type2' => $poke[3])
						)
					));
					endforeach;
					?>

				</form>
				<hr>

				
			
		</div>

	</div>

	 

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>		
<script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
</body>

</html>